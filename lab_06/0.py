import psycopg2
import json

class Artist:
    
    def __init__(self, id, firstname, lastname, country, birthdate):
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.country = country
        self.birthdate = birthdate

def loadDataFromJSON(dataFile):
    jsonFile = open(dataFile, "r")
    dataFromDB = json.load(jsonFile)
    jsonFile.close()
    
    return dataFromDB

def updateJSON(dataFile, dataFromDB, rowInd, colName, newValue):
    dataFromDB[rowInd][colName] = newValue
    jsonFile = open(dataFile, "w")

    json.dump(dataFromDB, jsonFile)
    jsonFile.close()

def addToJSON(dataFile, dataToDB):
    jsonFile = open(dataFile, "r")
    dataFromDB = json.load(jsonFile)
    jsonFile.close()

    dataFromDB.append(dataToDB)

    jsonFile = open(dataFile, "w")
    json.dump(dataFromDB, jsonFile)
    jsonFile.close()

# Однотабличный запрос
# Песни с количеством прослушивания меньше 1000
def doRequestType1():
    cur = con.cursor()  
    cur.execute('''select songid, listenings
                    from music_service
                    where listenings < 1000
                    order by listenings;''')

    rows = cur.fetchall()
    for row in rows:
        print(row)

# Многотабличный запрос
# Пользователи, которые не прослушали ни одну песню
def doRequestType2():
    cur = con.cursor()  
    cur.execute('''select *
                    from users
                    where not exists (
                        select songid
                        from music_service
                        where users.id = userid
                    );''')

    rows = cur.fetchall()
    for row in rows:
        print(row)

# Запросы на добавление, изменение и удаление
def doRequestType3():
    cur = con.cursor()
    cur.execute('''insert into artists(id, firstname, lastname, country, birthdate)
                    values(%s, %s, %s, %s, %s)''', \
                        [1006, "A", "A", "new country", "2014-03-15"])
    con.commit()

    # Если количество прослушиваний больше 5000, то песня скачена
    cur = con.cursor()
    cur.execute('''update music_service
                    set downloaded = true
                    where listenings > 5000;''')
    con.commit()

    # Удаление песни с именем Unnamed
    cur = con.cursor()
    cur.execute('''delete from songs
                    where title = 'Unnamed';''')
    con.commit()

# Доступ к данным, используя хранимую процедуру
# Количество прослушанных песен опрделённого пользователя
def doRequestType4():
    cur = con.cursor()  
    cur.execute("select * from get_count_songs(%s)", [4])

    rows = cur.fetchall()
    for row in rows:
        print(row)

if __name__ == "__main__":
    jsonFile = "/Users/mac-home/Desktop/DataBase/lab_06/0.json"
    p = "/Users/mac-home/Desktop/DataBase/p.txt"
    pf = open(p, "r")
    pfd = pf.readline().split("\n")[0]

    artists = loadDataFromJSON(jsonFile)

    updateJSON(jsonFile, artists, len(artists) - 1, "country", "Just country")

    dataToDB = {"id": 1005, "firstname": "A", "lastname": "A", \
        "country": "New country", "birthdate": "2014-03-15"}
    addToJSON(jsonFile, dataToDB)

    con = psycopg2.connect(
    database = "lab_03", 
    user = "postgres", 
    password = pfd, 
    host = "127.0.0.1", 
    port = "5432"
    )

    # doRequestType1()
    # doRequestType2()
    # doRequestType3()
    # doRequestType4()
    
    con.commit()  
    con.close()

    pf.close()
























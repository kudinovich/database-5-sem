--use master
use aero

set @autocommit = 0;
BULK INSERT Passengers
FROM 'C:\Users\�������\repos\database-5-sem\lab1\generate_data\output\passengers'
WITH (BATCHSIZE=1,CODEPAGE=65001, DATAFILETYPE='char', FIELDTERMINATOR=',', ROWTERMINATOR='\n', CHECK_CONSTRAINTS)
--select * from

BULK INSERT Trips
FROM 'C:\Users\�������\repos\database-5-sem\lab1\generate_data\output\trips'
WITH (CODEPAGE=65001, DATAFILETYPE='char', FIELDTERMINATOR=',', ROWTERMINATOR='\n')
--select * from Trips

BULK INSERT Airpark
FROM 'C:\Users\�������\repos\database-5-sem\lab1\generate_data\output\airpark'
WITH (CODEPAGE=65001, DATAFILETYPE='char', FIELDTERMINATOR=',', ROWTERMINATOR='\n')
--select * from Airpark


BULK INSERT Tickets
FROM 'C:\Users\�������\repos\database-5-sem\lab1\generate_data\output\tickets'
WITH (CODEPAGE=65001, DATAFILETYPE='char', FIELDTERMINATOR=',', ROWTERMINATOR='\n')
--select * from Tickets
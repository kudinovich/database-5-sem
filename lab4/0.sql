CREATE LANGUAGE plpython3u;

-- Скалярная функция
-- Найти большее число из двух

create or replace function pymax(a integer, b integer)
returns integer as $$
	if a > b:
		return a
	return b
$$ language plpython3u;

select id, listenings
from music_service
where listenings = pymax(listenings, 8000)
order by listenings;


-- Агрегатная функция
-- Найти максимальное количество прослушиваний

create or replace function findMax(arr integer[])
returns integer
as $$
	maxVal = 0
	
	for elem in arr:
		if (elem > maxVal):
			maxVal = elem
			
	return maxVal
$$ language plpython3u;

create or replace function arrayAppend(arr integer[], a integer)
returns integer[]
as $$
	arr.append(a)
	return arr
$$ language plpython3u;

create aggregate maxListenings (
	sfunc = arrayAppend,
	basetype = integer,
	stype = integer[],
	INITCOND = '{}',
	finalfunc = findMax
);

select maxListenings(listenings)
from music_service;


-- Табличная функция
-- Найти строки с нечётным кличеством прослушиваний

create or replace function getOddListenings()
returns table(id int, listenings int) as $$
	plan = plpy.prepare("select id, listenings from music_service where listenings % $1 <> 0", ["integer"])
	rows = list(plpy.cursor(plan, [2]))
	return rows
$$ language plpython3u;

select getOddListenings();


-- Хранимая процедура
-- Инкрементировать счётчик прослушиваний для определённого пользователя и песни

create or replace procedure incListenings(user_id integer, song_id integer)
language plpython3u as $$
	plan = plpy.prepare(
		"""update music_service
		set listenings = listenings + 1
		where userid = $1 and songid = $2""", ["integer", "integer"])
	
	plpy.execute(plan, [user_id, song_id])
$$;

call incListenings(4, 751);


-- Триггер
-- Тригер на логирование при добавлении в таблицу музыкантов

drop table logs;

create table logs (
	text varchar(160)
);

create or replace function addLog()
returns trigger as $$
	new_str = TD["new"]["country"]
	msg = 'Add new country: '
	res = msg + new_str
	
	plan = plpy.prepare(
		"insert into logs(text) values ($1)", ["text"])
	
	plpy.execute(plan, [res])
$$ language plpython3u;

drop trigger checkInsert on artists;

create trigger checkInsert
	after insert on artists for each row
	execute procedure addLog();

insert into artists(id, firstname, lastname, country, birthdate)
values (1004, 'A', 'A', 'New country', '2014-03-15');

select * from artists where id = 1004;

select * from logs;


-- Тип данных
-- Проверка пользователя

create type userType as (
	userid integer,
	listenings integer
);

create or replace function isAdvancedUser(ut userType)
returns boolean as $$
	if (ut["user_id"] > 4000 or ut["listenings"] > 8000):
		return True
	return False
$$ language plpython3u;

drop function makePair(user_id integer, listenings integer);

select isAdvancedUser(row(4, 751));









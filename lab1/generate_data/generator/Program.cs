﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace generator
{
    class Program
    {
        static void Main(string[] args)
        {
            gens.TablePassenger.gen();
            gens.TableAirpark.gen();
            gens.TableTrips.gen();
            gens.TableTickets.gen();

            Console.ReadKey();
        }
    }
}

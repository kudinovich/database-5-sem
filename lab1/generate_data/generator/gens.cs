﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace gens
{
    class TablePassenger
    {
        public static void gen()
        {
            string original = Environment.CurrentDirectory;
            Environment.CurrentDirectory = "../../../data";


            const int n = 40;
            StreamReader file;

            file = new StreamReader("names");
            string[] names = new string[n];
                for (int i = 0; i<n; i++)
                    names[i] = file.ReadLine();
                file.Close();
            
                
                file = new StreamReader("surnames");
            string[] surnames = new string[n];
                for (int i = 0; i<n; i++)
                    surnames[i] = file.ReadLine();
                file.Close();

                Random rnd = new Random();
            StreamWriter file_pas = new StreamWriter("../output/passengers", false);
                for (int i = 0; i<n; i++)
                    for (int j = 0; j<n; j++)
                        file_pas.WriteLine($",{names[i]},{surnames[j]},{rnd.Next(1, 100)}");
                file_pas.Close();

            Environment.CurrentDirectory = original;
        }
    }

    class TableAirpark
    {
        public static void gen()
        {
            string original = Environment.CurrentDirectory;
            Environment.CurrentDirectory = "../../../data";

            const int n2 = 7;
            StreamReader file = new StreamReader("aircompanies");
            string[] aircompanies = new string[n2];
            for (int i = 0; i < n2; i++)
                aircompanies[i] = file.ReadLine();
            file.Close();

            const int n3 = 16;
            file = new StreamReader("planes");
            string[] planes = new string[n3];
            for (int i = 0; i < n3; i++)
                planes[i] = file.ReadLine();
            file.Close();

            Random rnd = new Random();
            StreamWriter airpark = new StreamWriter("../output/airpark", false);
            for (int i = 0; i < 20; i++)
                airpark.WriteLine($",{planes[rnd.Next(0, n3 - 1)]},{rnd.Next(1990, 2019)},{rnd.Next(1, 5)},{aircompanies[rnd.Next(0, n2 - 1)]}");
            airpark.Close();

            Environment.CurrentDirectory = original;

        }
    }

    class TableTrips
    {
        public static void gen()
        {
            string original = Environment.CurrentDirectory;
            Environment.CurrentDirectory = "../../../data";

            const int n1 = 15;
            StreamReader file = new StreamReader("cities");
            string[] cities = new string[n1];
            for (int i = 0; i < n1; i++)
                cities[i] = file.ReadLine();
            file.Close();

      
            Random rnd = new Random();
            StreamWriter trips = new StreamWriter("../output/trips", false);
            for (int i = 0; i < n1; i++)
                for (int j = 0; j < n1; j++)
                    if (i != j)
                    {

                        int h1, h2;
                        int m1, m2;
                        h1 = rnd.Next(0, 23);
                        h2 = rnd.Next(0, 23);
                        m1 = rnd.Next(0, 59);
                        m2 = rnd.Next(0, 59);
                        if (Math.Abs(h1 - h2) < 2)
                        {
                            if (Math.Max(h1, h2) == h1)
                            {
                                h1 += 2;
                                h1 %= 10;
                            }
                            else
                            {
                                h2 += 2;
                                h2 %= 24;
                            }
                        }

                        trips.WriteLine($",{cities[i]},{cities[j]},{h1}:{m1},{h2}:{m2}");
                    }
            trips.Close();

            Environment.CurrentDirectory = original;

        }
    }

    class TableTickets
    {
        public static void gen()
        {
            string original = Environment.CurrentDirectory;
            Environment.CurrentDirectory = "../../../output";



            Random rnd = new Random();
            StreamWriter tickets = new StreamWriter("tickets", false);
            const int n1 = 1600;
            int[] array = new int[n1];
            for (int i = 0; i < n1; i++)
                array[i] = i + 1;

            for (int year = 2005; year <= 2015; year++)
                for (int month = 1; month <= 12; month++)
                    for(int day = 1; day <= 28; day += 2)
                    {
                        array = array.OrderBy(x => rnd.Next()).ToArray();
                        int trip = rnd.Next(1, 210);
                        int plane = rnd.Next(1, 20);
                        for (int i = 0; i <= rnd.Next(40, 90); i++)
                            tickets.WriteLine($",{array[i]},{trip},{plane},{year}-{month}-{day},{(i + 1) / 10 + 1}{(char)('A' + i % 10)}");

                    }

           


             
            tickets.Close();

            Environment.CurrentDirectory = original;

        }
    }



}

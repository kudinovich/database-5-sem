if EXISTS(select name from sys.databases where name = 'aero')
	use master 
	DROP DATABASE aero

GO
CREATE DATABASE aero
GO
use aero

CREATE TABLE Passengers
(
	id INT PRIMARY KEY IDENTITY,
	name nvarchar(20),
	surname nvarchar(20),
	age INT
)

CREATE TABLE trips
(
	id INT PRIMARY KEY IDENTITY,
	town_from varchar(20),
	town_to varchar(20),
	time_out time,
	time_in time
)

CREATE TABLE tickets
(
	id INT PRIMARY KEY IDENTITY,
	passenger_id INT,
	trip_id INT,
	plane_id INT,
	departure date,
	seat char(3)
)

CREATE TABLE airpark
(
	id INT PRIMARY KEY IDENTITY,
	name VARCHAR(20),
	year date,
	condition INT,
	aerocompany VARCHAR(20)
)


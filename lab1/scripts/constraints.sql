--use master

use aero

--�������� ����������� �� ��������
ALTER TABLE Passengers ADD CONSTRAINT cns_passenger_age CHECK(age BETWEEN 1 AND 150)
ALTER TABLE trips ADD CONSTRAINT cns_trips_town CHECK(town_from != town_to)
ALTER TABLE airpark ADD CONSTRAINT cns_airpark_year CHECK(year BETWEEN '1990-00-00' AND '2019-00-00')
ALTER TABLE airpark ADD CONSTRAINT cns_airpark_cnd CHECK(condition BETWEEN 1 AND 5)
ALTER TABLE tickets ADD CONSTRAINT cns_ticket_seat CHECK(seat LIKE '[0-9][a-z]' OR seat LIKE '[0-9][0-9][a-z]')
--ALTER TABLE ticket ADD CONSTRAINT cns_ticker_seat2(seat not in (select seat from ticket t2 where t2.departure = departure))

--�������� ������� ����� (������)
ALTER TABLE tickets ADD 
	CONSTRAINT fk_ticket_passenger FOREIGN KEY (passenger_id) REFERENCES Passengers(id),
	CONSTRAINT fk_ticket_trips FOREIGN KEY (trip_id) REFERENCES trips(id),
	CONSTRAINT fk_ticket_airpark FOREIGN KEY (plane_id) REFERENCES airpark(id)




